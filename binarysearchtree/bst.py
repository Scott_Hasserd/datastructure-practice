#! /bin/python3

class Node:

    left = None
    right = None
    parent = None

    def __init__(self, value):
        self.val = value


class binary_search_tree:

    def __init__(self, head: Node):
        self.head = head

    def add(self, value, node: Node):
        next = Node(value)
        if value <= node.val:
            if node.left is not None:
                self.add(value, node.left)
            else:
                node.left = next
                next.parent = node
        else:
            if node.right is not None:
                self.add(value, node.right)
            else:
                node.right = next
                next.parent = node

    def __post_order(self, node: Node, arr: list) -> list:
        if node:

            self.__post_order(node.left, arr)

            self.__post_order(node.right, arr)

            arr.append(node.val)

    def __pre_order(self, node: Node, arr: list) -> list:
        if node:

            arr.append(node.val)

            self.__pre_order(node.left, arr)

            self.__pre_order(node.right, arr)

    def __in_order(self, node: Node, arr: list) -> list:
        if node:
            self.__in_order(node.left, arr)

            arr.append(node.val)

            self.__in_order(node.right, arr)

    def post_order(self) -> list:
        arr = []

        self.__post_order(self.head, arr)

        return arr

    def pre_order(self) -> list:
        arr = []

        self.__pre_order(self.head, arr)

        return arr

    def in_order(self) -> list:
        arr = []

        self.__in_order(self.head, arr)

        return arr
