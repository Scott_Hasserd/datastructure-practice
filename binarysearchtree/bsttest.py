#! /bin/python3

import bst
import numpy as np

tree = bst.binary_search_tree(bst.Node(37))

print("Test 1 PASSED") if tree.head.val == 37 else print("Test 1 FAILED")

# test L/R insert
tree.add(3, tree.head)
print("Test 2 PASSED") if tree.head.left.val == 3 else print("Test 2 FAILED")
tree.add(40, tree.head)
print("Test 3 PASSED") if tree.head.right.val == 40 else print("Test 3 FAILED")

# test next level of insert
tree.add(2, tree.head)
print("Test 4 PASSED") if tree.head.left.left.val == 2 else print("Test 4 FAILED")
tree.add(10, tree.head)
print("Test 5 PASSED") if tree.head.left.right.val == 10 else print("Test 5 FAILED")
tree.add(44, tree.head)
print("Test 6 PASSED") if tree.head.right.right.val == 44 else print("Test 6 FAILED")
tree.add(39, tree.head)
print("Test 7 PASSED") if tree.head.right.left.val == 39 else print("Test 7 FAILED")

in_order = tree.in_order()
# print(in_order)
print("Test 8 PASSED") if np.array_equiv(in_order, [2, 3, 10, 37, 39, 40, 44]) else print("Test 8 FAILED")

pre_order = tree.pre_order()
# print(pre_order)
print("Test 9 PASSED") if np.array_equiv(pre_order, [37, 3, 2, 10, 40, 39, 44]) else print("Test 9 FAILED")

post_order = tree.post_order()
# print(post_order)
print("Test 10 PASSED") if np.array_equiv(post_order, [2, 10, 3, 39, 44, 40, 37]) else print("Test 10 FAILED")
