#include <iostream>
#include <array>

using std::cout, std::endl, std::array;

template <typename T, size_t N>
array<T, N> bubblesort(array<T, N> arr, int len){
  if (len == 0){
    return arr;
  }
  
  for(long unsigned int i = 0; i < len - 1; i++){
    if(arr[i] > arr[i+1]){
      T tmp = arr[i];
      arr[i] = arr[i+1];
      arr[i+1] = tmp;
    }
  }
  return bubblesort(arr, --len);
}
