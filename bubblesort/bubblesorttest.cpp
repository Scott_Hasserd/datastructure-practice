#include <iostream>
#include <array>

#include "bubblesort.cpp"

#include <assert.h>

using std::cout, std::endl, std::array;

void print(array<int, 5> arr){
  cout << "[ ";
  for(size_t i = 0; i < arr.size(); i++)
    cout << arr[i] << " ";
  cout << " ]" << endl;
}

int main(){

  array<int, 5> arr = {5, 4, 1, 55, 9};

  array<int, 5> ans = bubblesort(arr, arr.size());
  array<int, 5> test1 = {1, 4, 5, 9, 55};
  
  print(test1);
  print(ans);
  assert(test1 == ans);
  cout << "Test 1 PASSED" << endl;
  
}
