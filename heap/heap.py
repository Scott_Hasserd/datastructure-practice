#! /bin/python3

class max_heap:
    heap = []
    length = 0

    def __parent(self, pos):
        return (pos - 1) // 2

    def __left_child(self, pos):
        return (pos * 2) + 1

    def __right_child(self, pos):
        return (pos * 2) + 2

    def __enqueue(self, pos):
        while pos > 0 and self.heap[self.__parent(pos)] < self.heap[pos]:
            self.heap[self.__parent(pos)], self.heap[pos] = self.heap[pos], self.heap[self.__parent(pos)]
            pos = self.__parent(pos)

    def __dequeue(self, pos):
        maxPos = pos

        left = self.__left_child(pos)
        right = self.__right_child(pos)

        if left < self.length and self.heap[left] > self.heap[maxPos]:
            maxPos = left

        if right < self.length and self.heap[right] > self.heap[maxPos]:
            maxPos = right

        if pos != maxPos:
            self.heap[pos], self.heap[maxPos] = self.heap[maxPos], self.heap[pos]
            self.__dequeue(maxPos)

    def enqueue(self, val):

        self.heap.append(val)

        self.__enqueue(self.length)

        self.length += 1

    def dequeue(self):
        if self.length == 1:
            self.length = 0
            return self.heap.pop()

        self.length -= 1

        ret_val = self.heap[0]
        self.heap[0] = self.heap.pop()

        self.__dequeue(0)

        return ret_val

    def find(self, val):
        pass
