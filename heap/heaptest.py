#! /bin/python3

import heap as hp

priority_queue = hp.max_heap()

print("Test 1 PASSED") if priority_queue.heap == [] else print("Test 1 FAILED")

priority_queue.enqueue(5)

print("Test 2 PASSED") if priority_queue.length == 1 else print("Test 2 FAILED")
print("Test 3 PASSED") if priority_queue.heap[0] == 5 else print("Test 3 FAILED")

# add smaller number
priority_queue.enqueue(3)
print("Test 4 PASSED") if priority_queue.length == 2 else print("Test 4 FAILED")
print("Test 5 PASSED") if priority_queue.heap[1] == 3 else print("Test 5 FAILED")
priority_queue.enqueue(4)
print("Test 6 PASSED") if priority_queue.length == 3 else print("Test 6 FAILED")
print("Test 7 PASSED") if priority_queue.heap[2] == 4 else print("Test 7 FAILED")

# add larger number
priority_queue.enqueue(9)
print("Test 8 PASSED") if priority_queue.length == 4 else print("Test 8 FAILED")
print("Test 9 PASSED") if priority_queue.heap[0] == 9 else print("Test 9 FAILED")
priority_queue.enqueue(15)
print("Test 10 PASSED") if priority_queue.length == 5 else print("Test 10 FAILED")
print("Test 11 PASSED") if priority_queue.heap[0] == 15 else print("Test 11 FAILED")

# testing dequeue
priority_queue.dequeue()
print("Test 10 PASSED") if priority_queue.length == 4 else print("Test 10 FAILED")
print("Test 11 PASSED") if priority_queue.heap[0] == 9 else print("Test 11 FAILED")
print("Test 12 PASSED") if priority_queue.heap[1] == 5 else print("Test 12 FAILED")

priority_queue.dequeue()
print("Test 13 PASSED") if priority_queue.length == 3 else print("Test 13 FAILED")
print("Test 14 PASSED") if priority_queue.heap[0] == 5 else print("Test 14 FAILED")


priority_queue.enqueue(4)
priority_queue.enqueue(44)
priority_queue.enqueue(18)
priority_queue.enqueue(32)
priority_queue.enqueue(29)
priority_queue.enqueue(1)
priority_queue.enqueue(77)
priority_queue.enqueue(12)
priority_queue.enqueue(14)

while priority_queue.length > 0:
    print(priority_queue.dequeue())

print(priority_queue.heap)
