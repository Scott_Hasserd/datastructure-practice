#! /bin/python3

class LinkedList():

    head = None
    tail = None
    length = 0

    def append(self, node):
        if self.length == 0:
            self.head = self.tail = node
            self.length += 1
            return
        self.tail.nextNode = node
        self.tail.nextNode.prevNode = self.tail
        self.tail = node
        self.length += 1

    def prepend(self, node):
        if self.length == 0:
            self.head = self.tail = node
            self.length += 1
            return
        self.head.prevNode = node
        node.nextNode = self.head
        self.head = node
        self.length += 1

    def __findHelper(self, val, node, idx):
        if node is None:
            return -1
        idx += 1
        if node.getVal() == val:
            return idx
        return self.__findHelper(val, node.nextNode, idx)

    def find(self, val):
        idx = -1
        if self.head.getVal() == val:
            idx = 0
            return idx
        return self.__findHelper(val, self.head.nextNode, 0)

    def getAt(self, idx):
        if idx > self.length or idx < 0:
            return None
        curr = self.head
        i = 0
        while i < idx:
            curr = curr.nextNode
            i += 1
        return curr

    def insertAt(self, idx, node):
        if idx <= 0:
            self.prepend(node)
        elif idx >= self.length:
            self.append(node)
        else:
            curr = self.getAt(idx)
            prev = curr.prevNode
            node.nextNode = curr
            node.prevNode = prev
            prev.nextNode = node
            curr.prevNode = node



class Node():

    value = None
    nextNode = None
    prevNode = None

    def __init__(self, val):
        self.value = val

    def printVal(self):
        print(self.value)

    def getVal(self):
        return self.value
