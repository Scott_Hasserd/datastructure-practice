#! /bin/python3

import linkedList as ll

# test node works
test1 = ll.Node(4)
print("Test #1 PASSED") if test1.getVal() == 4 else print("Test #1 FAILED")

test2 = ll.LinkedList()
test2.append(test1)
print("Test #2.1 PASSED") if test2.head.getVal() == 4 else print("Test #2.1 FAILED")
print("Test #2.2 PASSED") if test2.tail.getVal() == 4 else print("Test #2.2 FAILED")
print("Test #2.3 PASSED") if test2.length == 1 else print("Test #2.3 FAILED")

test3 = ll.Node(90)
test2.append(test3)
print("Test #3.1 PASSED") if test2.length == 2 else print("Test #3.1 FAILED")
print("Test #3.2 PASSED") if test2.head.getVal() == 4 else print("Test #3.2 FAILED")
print("Test #3.3 PASSED") if test2.tail.getVal() == 90 else print("Test #3.3 FAILED")

test4 = ll.Node(10)
test2.append(test4)
print("Test #4.1 PASSED") if test2.length == 3 else print("Test #4.1 FAILED")
print("Test #4.2 PASSED") if test2.head.getVal() == 4 else print("Test #4.2 FAILED")
print("Test #4.3 PASSED") if test2.head.nextNode.getVal() == 90 else print("Test #4.3 FAILED")
print("Test #4.4 PASSED") if test2.tail.getVal() == 10 else print("Test #4.4 FAILED")

test2.prepend(ll.Node(60))
print("Test #5.1 PASSED") if test2.length == 4 else print("Test #5.1 FAILED")
print("Test #5.2 PASSED") if test2.head.getVal() == 60 else print("Test #5.2 FAILED")
print("Test #5.3 PASSED") if test2.head.nextNode.getVal() == 4 else print("Test #5.3 FAILED")
print("Test #5.4 PASSED") if test2.tail.getVal() == 10 else print("Test #5.4 FAILED")

# testing find
print("Test #6.1 PASSED") if test2.find(60) == 0 else print("Test #6.1 FAILED")
print("Test #6.2 PASSED") if test2.find(4) == 1 else print("Test #6.2 FAILED")
print("Test #6.3 PASSED") if test2.find(44) == -1 else print("Test #6.3 FAILED")

print("Test #7.1 PASSED") if test2.getAt(1).getVal() == 4 else print("Test #7.1 FAILED")
print("Test #7.2 PASSED") if test2.getAt(0).getVal() == 60 else print("Test #7.2 FAILED")
print("Test #7.3 PASSED") if test2.getAt(3).getVal() == 10 else print("Test #7.3 FAILED")
print("Test #7.4 PASSED") if test2.getAt(4) is None else print("Test #7.4 FAILED")
print("Test #7.5 PASSED") if test2.getAt(-1) is None else print("Test #7.5 FAILED")

test2.insertAt(3, ll.Node(77))
print("Test #8.1 PASSED") if test2.find(77) == 3 else print("Test #8.1 FAILED")
test2.insertAt(-3, ll.Node(88))
print("Test #8.2 PASSED") if test2.find(88) == 0 else print("Test #8.2 FAILED")
test2.insertAt(10, ll.Node(95))
print("Test #8.3 PASSED") if test2.tail.getVal() == 95 else print("Test #8.3 FAILED")
