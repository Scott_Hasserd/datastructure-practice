#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

template<typename T>
size_t partition(vector<T>* vec, int lo, int hi){
	T pivot = (*vec)[hi];

	int higher = lo - 1;

	for(int i = lo; i < hi; i++){
		if((*vec)[i] < pivot){
			higher++;
			T tmp = (*vec)[i];
			(*vec)[i] = (*vec)[higher];
			(*vec)[higher] = tmp;
		}

	}

		higher++;
		T tmp = (*vec)[hi];
		(*vec)[hi] = (*vec)[higher];
		(*vec)[higher] = tmp;
		return higher;
}

template<typename T>
void quicksort(vector<T>* vec, int lo, int hi){
	if(lo >= hi)
		return;

	int piv = partition(vec, lo, hi);

	quicksort(vec, lo, piv - 1);

	quicksort(vec, piv + 1, hi);


}

template<typename T>
void print(vector<T> items){
	cout << "[ ";

	for(T itm : items){
		cout << itm << ", ";
	}

	cout << "]" << endl;
}

int main(){
	vector<int> arr = {5, 2, 88, 1, 4};
	vector<int> test = {1, 2, 4, 5, 88};

	quicksort(&arr, 0, arr.size() - 1);

	print(arr);

	if(arr == test)
		cout << "PASSED" << endl;
	else
		cout << "FAILED" << endl;
}
