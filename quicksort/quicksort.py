#! /bin/python3
import math


def partition(arr: list, low: int, high: int):
    pivot = arr[high]

    higher = low - 1

    for j in range(low, high):
        if arr[j] <= pivot:
            higher = higher + 1
            (arr[higher], arr[j]) = (arr[j], arr[higher])

    higher += 1

    (arr[higher], arr[high]) = (arr[high], arr[higher])

    return higher


def quicksort(arr: list, low: int, high: int) -> list:
    if low < high:

        piv = partition(arr, low, high)

        quicksort(arr, low, piv - 1)

        quicksort(arr, piv + 1, high)
