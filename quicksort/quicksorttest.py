#! /bin/python3
import quicksort as qs


arr = [5, 8, 99, 1, 4, 3]
test1 = [1, 3, 4, 5, 8, 99]

qs.quicksort(arr, 0, len(arr) - 1)

print(arr)

print("Test 1 PASSED") if test1 == arr else print("Test 2 FAILED")
