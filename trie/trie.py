#! /bin/python3


class Node:

    def __init__(self, val):
        self.char = val
        self.is_word = False
        self.children = {}


class Trie:

    def __init__(self):
        self.head = Node(None)

    def add(self, word: str):

        node = self.head

        for char in word:
            if char in node.children:
                node = node.children[char]
            else:

                new_node = Node(char)
                node.children[char] = new_node
                node = new_node

        node.is_word = True

    def dfs(self, node: Node, pre: list):

        if node.is_word:
            self.output.append((pre + node.char))

        for child in node.children.values():
            self.dfs(child, pre + node.char)

    def has(self, word: str):

        node = self.head
        word = word.lower()
        for char in word:
            if char in node.children:
                node = node.children[char]
            else:

                return False

        self.output = []
        self.dfs(node, word[:-1])

        if word in self.output:
            return True
        else:
            return False
