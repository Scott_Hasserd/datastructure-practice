#! /bin/python3

import sys
import trie as tr


class bcolors:
    FAIL = '\033[91m'
    ENDC = '\033[0m'


vtree = tr.Trie()

dictionary = open("words_alpha.txt", "r")

for line in dictionary:
    vtree.add(line.strip())

#print(vtree.has("apple"))
#print(vtree.has("tree"))
#print(vtree.has("computer"))
#print(vtree.has("xylophone"))
#print(vtree.has("zoo"))
#print("---------")
#if not vtree.has("adfxa"):
#    print(bcolors.FAIL + "adfxa" + bcolors.ENDC)
#print(vtree.has("fartface"))
#print(vtree.has("kdkdkjsjs"))

file2check = open(sys.argv[1], "r")
for line in file2check:
    if line == "\n":
        print("");
        continue
    line_arr = line.split(" ")
    for word in line_arr:
        if vtree.has(word.strip()):
            if "\n" in word:
                print(word, end="")
            else:
                print(word, end=" ")
        else:
            if "\n" in word:
                print(bcolors.FAIL + word + bcolors.ENDC, end="")
            else:
                print(bcolors.FAIL + word + bcolors.ENDC, end=" ")


dictionary.close()
